from django.db import models

from shelters.models import Shelter


class Pet(models.Model):
    """Pet model"""

    CAT = 'cat'
    DOG = 'dog'
    BIRD = 'bird'
    HORSE = 'horse'
    WILDLIFE = 'wildlife'

    PURE = 'pure'
    MIXED = 'mixed'

    SMALL = 'small'
    MEDIUM = 'medium'
    LARGE = 'large'

    MALE = 'male'
    FEMALE = 'female'

    SPECIES_CHOICES = (
        (CAT, 'Cat'),
        (DOG, 'Dog'),
        (BIRD, 'Bird'),
        (WILDLIFE, 'Wildlife')
    )

    BREED_CHOICES = (
        (PURE, 'Pure'),
        (MIXED, 'Mixed')
    )

    SIZE_CHOICES = (
        (SMALL, 'Small'),
        (MEDIUM, 'Medium'),
        (LARGE, 'Large')
    )

    GENDER_CHOICES = (
        (MALE, 'Male'),
        (FEMALE, 'Female')
    )

    shelter = models.ForeignKey(Shelter, on_delete=models.CASCADE)
    photo = models.ImageField('Photo', upload_to='animal_photos/', blank=True, null=True)
    name = models.CharField('Pet name', max_length=30)
    gender = models.CharField('Gender', choices=GENDER_CHOICES, max_length=10)
    species = models.CharField('Species', choices=SPECIES_CHOICES, max_length=20)
    breed = models.CharField('Breed', choices=BREED_CHOICES, max_length=20)
    age = models.PositiveIntegerField('Age', null=True, blank=True)
    size = models.CharField('Size', choices=SIZE_CHOICES, max_length=10)
    tag = models.CharField(max_length=255, blank=True, null=True)
    microchip = models.PositiveIntegerField('Microchip', null=True, blank=True)
    social = models.BooleanField('Social')
    accommodation = models.BooleanField('Accommodation')

    def __str__(self):
        return f'{self.shelter} - {self.species} - {self.name}'
