from rest_framework.viewsets import ModelViewSet

from .models import Pet
from .serializers import PetSerializer
from accounts.permissions import RelatedGroupPermissions


class PetViewSet(ModelViewSet):
    """Pet ViewSet"""

    queryset = Pet.objects.all()
    serializer_class = PetSerializer
    permission_classes = [RelatedGroupPermissions]

    def get_queryset(self):
        return self.queryset.filter(shelter=self.request.user.shelter_id)
