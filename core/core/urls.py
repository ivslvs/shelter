from django.contrib import admin
from django.urls import path, include
from rest_auth.views import PasswordResetConfirmView

from accounts.views import CustomLoginView, CustomPasswordReset

urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/', include([
        path('accounts/', include('accounts.urls'), name='accounts'),
        path('shelters/', include('shelters.urls'), name='shelters'),
        path('pets/', include('pets.urls'), name='pets'),

        path('rest-auth/', include('rest_auth.urls')),

        path('login/', CustomLoginView.as_view(), name='login'),
        path('password/reset/', CustomPasswordReset.as_view(), name='password_reset'),
        path(
            'password/reset/confirm/(<uidb64>[0-9A-Za-z_\-]+)/(<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
            PasswordResetConfirmView.as_view(),
            name='password_reset_confirm'),

    ]),
         ),

]
