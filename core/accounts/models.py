from django.db import models
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth.models import Group

from .managers import UserManager
from shelters.models import Shelter


class User(AbstractUser):
    """Extended User model"""

    username = None
    shelter = models.ForeignKey(Shelter, on_delete=models.CASCADE, blank=True, null=True)
    email = models.EmailField('Email address', unique=True)
    first_name = models.CharField('First name', max_length=150, blank=True, null=True)
    last_name = models.CharField('Last name', max_length=150, blank=True, null=True)
    photo = models.ImageField('Photo', default="profile1.png", blank=True)
    phone = PhoneNumberField('Phone', unique=True, blank=True, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        permissions = [("can_reset_password", "Can reset password")]

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class CustomGroup(Group):
    """Custom group model"""

    shelter = models.ForeignKey(Shelter, on_delete=models.CASCADE)
