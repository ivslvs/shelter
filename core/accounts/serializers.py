from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission, Group
from rest_auth.serializers import LoginSerializer

from shelters.models import Shelter
from .models import CustomGroup


User = get_user_model()


class CustomLoginSerializer(LoginSerializer):
    username = None
    email = serializers.EmailField()
    organization = serializers.PrimaryKeyRelatedField(required=True, queryset=Shelter.objects.all())

    def validate(self, attrs):
        validated_attrs = super(CustomLoginSerializer, self).validate(attrs)
        email = validated_attrs['email']
        shelter = validated_attrs['organization']

        if not User.objects.filter(email=email, shelter=shelter):
            raise serializers.ValidationError({
                'message': ["User doesn't related to this shelter."]
            })
        return validated_attrs


class UserSerializer(ModelSerializer):
    shelter = serializers.PrimaryKeyRelatedField(required=True, queryset=Shelter.objects.all())

    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class PermissionSerializer(ModelSerializer):

    class Meta:
        model = Permission
        fields = '__all__'


class GroupSerializer(ModelSerializer):

    class Meta:
        model = CustomGroup
        fields = '__all__'


class ShelterAdminGroupSerializer(ModelSerializer):

    class Meta:
        model = Group
        fields = '__all__'
