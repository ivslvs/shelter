from rest_framework.routers import DefaultRouter

from .views import UserViewSet, GroupViewSet, ShelterAdminPermissionViewSet


app_name = 'accounts'

router = DefaultRouter()

router.register('users', UserViewSet, basename='user')
router.register('permissions', ShelterAdminPermissionViewSet, basename='permission')
router.register('groups', GroupViewSet, basename='group')

urlpatterns = [

]

urlpatterns += router.urls
