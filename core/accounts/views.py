from rest_framework.viewsets import ModelViewSet
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from rest_auth.views import LoginView, PasswordResetView
from rest_auth.serializers import PasswordResetSerializer


from .serializers import (
     UserSerializer, GroupSerializer, ShelterAdminGroupSerializer, CustomLoginSerializer
)
from .models import CustomGroup
from .permissions import RelatedGroupPermissions, PasswordResetPermissions


User = get_user_model()


class CustomLoginView(LoginView):
    """Custom Login with additional field 'organization'"""

    serializer_class = CustomLoginSerializer


class CustomPasswordReset(PasswordResetView):
    """Custom password reset with user permissions"""

    queryset = User.objects.all()
    serializer_class = PasswordResetSerializer
    permission_classes = [PasswordResetPermissions]


class UserViewSet(ModelViewSet):
    """User ViewSet"""

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [RelatedGroupPermissions]

    def get_queryset(self):
        return self.queryset.filter(shelter=self.request.user.shelter_id)


class ShelterAdminPermissionViewSet(ModelViewSet):
    """ViewSet of all Shelter admin permissions in order to grant it to custom groups"""

    queryset = Group.objects.all()
    serializer_class = ShelterAdminGroupSerializer
    permission_classes = [RelatedGroupPermissions]

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)


class GroupViewSet(ModelViewSet):
    """ViewSet of groups related to specific Shelter"""

    queryset = CustomGroup.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [RelatedGroupPermissions]

    def get_queryset(self):
        return self.queryset.filter(shelter=self.request.user.shelter_id)
