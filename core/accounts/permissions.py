import copy
from rest_framework.permissions import DjangoModelPermissions
from rest_framework import permissions


class RelatedGroupPermissions(DjangoModelPermissions):
    """Custom permissions"""

    def __init__(self):
        self.perms_map = copy.deepcopy(self.perms_map)
        self.perms_map['GET'] = ['%(app_label)s.view_%(model_name)s']


class PasswordResetPermissions(permissions.BasePermission):
    """Permission to reset password for specific user group"""

    required_perm = 'accounts.can_reset_password'

    def has_permission(self, request, view):
        user_perm = request.user.get_group_permissions()
        return self.required_perm in user_perm
