from django.apps import AppConfig


class ShelterConfig(AppConfig):
    name = 'shelters'
