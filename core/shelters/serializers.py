from rest_framework.serializers import ModelSerializer

from .models import Shelter


class ShelterSerializer(ModelSerializer):
    """Shelter serializer"""

    class Meta:
        model = Shelter
        fields = '__all__'
