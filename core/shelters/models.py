from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Shelter(models.Model):
    """Shelter model"""

    APPROVED = 'approved'
    DECLINED = 'declined'
    AWAITING = 'awaiting'
    DISABLED = 'disabled'

    STATUS_CHOICES = (
        (APPROVED, 'Approved'),
        (DECLINED, 'Declined'),
        (AWAITING, 'Awaiting'),
        (DISABLED, 'Disabled')
    )

    organization = models.IntegerField('Organization')
    name = models.CharField('Name', max_length=150)
    phone = PhoneNumberField('Phone', unique=True)
    email = models.EmailField('Email address', unique=True)
    url = models.URLField('URL', max_length=200, blank=True, null=True)
    notes = models.TextField('Notes', blank=True, null=True)
    logo = models.ImageField('Logo', upload_to='logos/', blank=True, null=True)
    header = models.ImageField('Header', upload_to='headers/', blank=True, null=True)
    status = models.CharField('Status', max_length=8, choices=STATUS_CHOICES, default=AWAITING)

    def __str__(self):
        return f'{self.organization} - {self.name}'
