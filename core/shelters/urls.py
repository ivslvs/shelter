from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import ShelterViewSet, ShelterRequestCreateAPIView


app_name = 'shelters'

router = DefaultRouter()

router.register('', ShelterViewSet, basename='shelter')

urlpatterns = [
    path('request/', ShelterRequestCreateAPIView.as_view(), name='request'),
]

urlpatterns += router.urls
