from rest_framework.viewsets import ModelViewSet
from django.contrib.auth import get_user_model
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny

from .models import Shelter
from .serializers import ShelterSerializer
from accounts.permissions import RelatedGroupPermissions


User = get_user_model()


class ShelterRequestCreateAPIView(CreateAPIView):
    """Shelter account request"""

    model = Shelter
    serializer_class = ShelterSerializer
    permission_classes = [AllowAny]


class ShelterViewSet(ModelViewSet):
    """Shelter ViewSet"""

    queryset = Shelter.objects.all()
    serializer_class = ShelterSerializer
    permission_classes = [RelatedGroupPermissions]
